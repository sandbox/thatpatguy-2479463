INTRODUCTION
------------
This module will set up the WET4 Carousel Tabbed Interface
(http://wet-boew.github.io/wet-boew/demos/tabs/tabs-carousel-en.html).

This module will create a new Content Type called "Carousel". This Content will
feature an upload field for the various slides, the ability to add alt and title
text to each slide, the ability to add an HTML caption to each slide, the
ability to reorder slides, as well as select which Carousel style and even some
basic slide resizing (no resize, 4:3 ratio, 16:9 ratio, etc)

Each Carousel created is it's own Node and the user can either use Panels or the
Node Embed module to place these nodes where they want in the site.

 * For a full description of the module, visit the project page:
 https://www.drupal.org/sandbox/thatpatguy/2479463

 * To submit bug reports and feature suggestions, or to track changes:
 https://www.drupal.org/sandbox/thatpatguy/2479463

REQUIREMENTS
------------
This module requires the following modules:

 * The Wet4 Theme (https://www.drupal.org/project/wet4)
 * Entity (https://www.drupal.org/project/entity)
 * Link (https://www.drupal.org/project/link)

INSTALLATION
------------
 * Install as usual, see http://drupal.org/node/895232 for further information.


CONFIGURATION
-------------
Once the module is installed you'll have a new Content Type named "Carousel".
In this content type you have the ability to set the aspect ratio of the slides
used in the carousel.  The four options are:

 1. No Change (use the ratio of the images as they are loaded by the user)
 2. Standard Photo (4:3 aspect ratio)
 3. Wide Screen (16:9 aspect ratio)
 4. Extreme Wide Screen (3:1 aspect ratio)

In order for these ratios to work, you need to build the following three Image
Styles:

  1. Standard Photo (4:3 aspect ratio):
  Set machine name to 'standard'
  Scale width 800 (upscaling allowed)
  Scale and crop 800x600

  2. Wide Screen (16:9 aspect ratio):
  Set machine name to 'wide'
  Scale width 800 (upscaling allowed)
  Scale and crop 800x450

  3. Extreme Wide Screen (3:1 aspect ratio):
  Set machine name to 'x-wide'
  Scale width 800 (upscaling allowed)
  Scale and crop 800x267


MAINTAINERS
-----------
Current maintainers:
 * Pat Cooney (thatpatguy)- https://www.drupal.org/u/thatpatguy

This project has been sponsored by:
 * OPIN Software Inc. OPIN is a provider of enterprise content management
 solutions built with Drupal. At OPIN, our vision is to assist clients through
 Drupal implementation and consulting and to help them achieve an effective web
 presence.
