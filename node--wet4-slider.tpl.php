<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */

/**
 * Carousel Types.
 *
 * Basic Buttons (displayed above the carousel)      => carousel-s1
 * Rounded Buttons (displayed below the carousel)    => carousel-s2
 * Display Thumbnails (displayed below the carousel) => carousel-s2 show-thumbs.
 *
 *
 * Transition Interval.
 *
 * Interval number is in seconds (not milliseconds).
 *
 *
 * Transition Style.
 *
 * Fade                 => class="in fade" class="out fade"
 * Slide Horizontal     => class="slide in" class="slide out"
 * Slide Vertical       => class="slidevert in" class="slidevert out"
 */

$node = entity_metadata_wrapper('node', $node);
$control_options = $node->field_control_options->value();
$carousel_interval = $node->field_carousel_interval->value();
$carousel_style = $node->field_carousel_style->value();
$carousel_items = $node->field_slides->value();
$carousel_ratio = $node->field_carousel_ratio->value();

$show_thumbs = 0;


if($control_options == "2"):
    $control_options = "carousel-s2";
elseif($control_options == "3"):
    $control_options = "carousel-s2 show-thumbs";
    $show_thumbs = 1;
else:
    $control_options = "carousel-s1";
endif;

$carousel_interval = (!empty($carousel_interval) ? 'data-wb-tabs=\'{"interval":' . $carousel_interval . '}\'' : '');

if($carousel_style == "2"):
    $carousel_style = "slide";
elseif($carousel_style == "3"):
    $carousel_style = "slidevert";
else:
    $carousel_style = "fade";
endif;

if($carousel_ratio == "2"):
    $carousel_ratio = "standard";
elseif($carousel_ratio == "3"):
    $carousel_ratio = "wide";
elseif($carousel_ratio == "4"):
    $carousel_ratio = "x_wide";
else:
    $carousel_ratio = "none";
endif;

$img_width = 0;
$img_height = 0;
$img_ratio = 0;

foreach ($carousel_items as $id => $carousel_slide) :
  // Original image uri.
  $path = file_load($carousel_slide['carousel_image'])->uri;
  // Machine-readable name of the style.
  $style = $carousel_ratio;
  // URL of the image style.
  // Use this within your template file by assigning it to $vars['style_url'].
  $style_url[$id] = ($carousel_ratio != 'none' ? image_style_url($style, $path) : file_create_url(file_load($carousel_slide['carousel_image'])->uri));
  $img_size = getimagesize($style_url[$id]);
  $img_ratio = $img_size[0] / 140;
  $img_width = ceil($img_size[0] / $img_ratio);

  if (ceil($img_size[0] / $img_ratio) <= $img_width):
      $img_width = ceil($img_size[0] / $img_ratio);
      $img_height = ($img_height > ceil($img_size[1] / $img_ratio) ? $img_height : ceil($img_size[1] / $img_ratio));
  endif;
endforeach;

$img_height += 20;

?>

<div class="wb-tabs <?php print $control_options;?>" <?php print $carousel_interval; ?> style="padding-bottom:<?php print $img_height; ?>px">
    <ul role="tablist">

        <?php foreach ($carousel_items as $id => $carousel_slide) :
            $slide_num = $id;
            $slide_num++;?>
            <li <?php $id == "0" ? print 'class="active"' : print ''; ?>><a href="#panel<?php print $slide_num; ?>">
            <?php $show_thumbs == 1 ? print '<img src="' . $style_url[$id] . '" alt="Tab ' . $slide_num . '" >' : print 'Tab ' . $slide_num; ?>
            </a></li>
        <?php endforeach; ?>

    </ul>

    <div class="tabpanels">

        <?php foreach ($carousel_items as $id => $carousel_slide) :
            $slide_num = $id;
            $slide_num++; ?>

            <div role="tabpanel" id="panel<?php print $slide_num; ?>" class="<?php $id == "0" ? print "in" : print "out"; ?> <?php print $carousel_style; ?>">
                <figure>
                    <?php $img_url = file_create_url(file_load($carousel_slide['carousel_image'])->uri); ?>
                    <?php !empty($carousel_slide['image_url_text']) ? print '<a href="' . check_url($carousel_slide['image_url_text']) . '">' : print ''; ?><img src="<?php print $style_url[$id]; ?>" alt="<?php print check_plain($carousel_slide['image_alt_text']);?>" title="<?php print check_plain($carousel_slide['image_title_text']);?>"><?php !empty($carousel_slide['image_url_text']) ? print '</a>' : print ''; ?>
                    <?php if (!empty($carousel_slide['image_caption'])):?>
                        <figcaption>
                            <?php print check_markup($carousel_slide['image_caption'], 'full_html', '', FALSE); ?>
                        </figcaption>
                    <?php endif; ?>
                </figure>
            </div>
        <?php endforeach; ?>


    </div>
</div>
